import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './components/Home';
import Board from './components/Board';
import NavBar from './components/NavBar';

function App() {
  return (
    <Router>
      <NavBar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/boards" element={<Home />} />
        <Route path="/boards/:boardId" element={<Board />} />
      </Routes>
    </Router>
  );
}

export default App;
