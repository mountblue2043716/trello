import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import List from "./List"; 
import { apiToken, apiKey } from "../../config";
import {
  Typography,
  Button,
  TextField,
} from "@mui/material";

function Board() {
  const { boardId } = useParams();
  const [boardName, setBoardName] = useState("");
  const [lists, setLists] = useState([]);
  const [newListName, setNewListName] = useState("");

  useEffect(() => {
    fetchLists();
  }, []);

  const fetchLists = async () => {
    try {
      const response = await axios.get(
        `https://api.trello.com/1/boards/${boardId}?lists=open&list_fields=name&key=${apiKey}&token=${apiToken}`
      );
      setBoardName(response.data.name);
      setLists(response.data.lists);
    } catch (error) {
      console.error("Error fetching board:", error);
    }
  };

  const createList = async () => {
    try {
      const response = await axios.post(
        `https://api.trello.com/1/lists?key=${apiKey}&token=${apiToken}`,
        { name: newListName, idBoard: boardId }
      );
      setLists(prevState => [...prevState, response.data]);
      setNewListName("");
    } catch (error) {
      console.error("Error creating list:", error);
    }
  };

  const deleteList = async (listId) => {
    try {
      let archiveUrl = `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${apiToken}`;
      await axios({
        method: "put",
        url: archiveUrl,
        data: {
          ...lists,
          closed: true,
        },
      });
      setLists(lists.filter((list) => listId !== list.id));
    } catch (error) {
      console.error("Error deleting list:", error);
    }
  };

  return (
    <div style={{ marginLeft: "30px" }}>
      <Typography variant="h4" gutterBottom>
        {boardName}
      </Typography>
      <div style={{ display: "flex", overflowX: "auto" }}>
        {lists.map((list) => (
          <List key={list.id} list={list} onDeleteList={deleteList}/>
        ))}
        <div className="create-list-container" style={{marginRight: "30px", marginLeft: "10px"}}>
          <div>
            <TextField
              label="New List Name"
              value={newListName}
              onChange={(e) => setNewListName(e.target.value)}
              sx={{ marginTop: "16px" }}
            />
          </div>
          <Button variant="contained" onClick={createList}>
            Create List
          </Button>
        </div>
      </div>
    </div>
  );
}

export default Board;
