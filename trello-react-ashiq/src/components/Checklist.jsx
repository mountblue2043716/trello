/* eslint-disable react/prop-types */
// import { Modal } from "@mui/material";
import { Typography, Card, CardContent, Button } from "@mui/material";
import { useState, useEffect } from "react";
import axios from "axios";
import CheckItem from "./CheckItem";
import { apiToken, apiKey } from "../../config";

const Checklist = ({ checklist, onDeleteChecklist }) => {
  const [checkItems, setCheckItems] = useState([]);
  const [newCheckItemName, setNewCheckItemName] = useState("");

  useEffect(() => {
    fetchCheckItems();
  }, []);

  const fetchCheckItems = async () => {
    try {
      const response = await axios.get(
        `https://api.trello.com/1/checklists/${checklist.id}/checkItems?key=${apiKey}&token=${apiToken}`
      );
      setCheckItems(response.data);
    } catch (error) {
      console.error("Error fetching check items:", error);
      return [];
    }
  };
  const deleteCheckItem = async (checkItemId) => {
    try {
      const response = await axios.delete(
        `https://api.trello.com/1/checkItems/${checkItemId}?key=${apiKey}&token=${apiToken}`
      );
      setCheckItems(
        checkItems.filter((checkItem) => checkItem.id !== checkItemId)
      );
      console.log("Check item deleted successfully:", response.data);
    } catch (error) {
      console.error("Error deleting check item:", error);
    }
  };

  return (
    <div>
      <Card sx={{ margin: "8px 0", backgroundColor: "lavender" }}>
        <CardContent>
          <Typography variant="body1">{checklist.name}</Typography>
          <Button
            onClick={(event) => {
              onDeleteChecklist(checklist.id, event);
            }}
          >
            Delete Checklist
          </Button>
          {checkItems.map((checkItem) => {
            <CheckItem
              key={checkItem.id}
              checkItem={checkItem}
              onDeleteCheckItem={deleteCheckItem}
            />;
          })}
        </CardContent>
      </Card>
    </div>
  );
};

export default Checklist;
