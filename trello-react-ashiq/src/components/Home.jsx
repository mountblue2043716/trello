import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { apiToken, apiKey } from "../../config";
import {
  Typography,
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
} from "@mui/material";

function Home() {
  const [boards, setBoards] = useState([]);
  const [newBoardName, setNewBoardName] = useState("");

  useEffect(() => {
    fetchBoards();
  }, []);

  const fetchBoards = async () => {
    try {
      const response = await axios.get(
        `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${apiToken}`
      );
      setBoards(response.data);
    } catch (error) {
      console.error("Error fetching boards:", error);
    }
  };

  const createBoard = async () => {
    try {
      const response = await axios.post(
        `https://api.trello.com/1/boards?key=${apiKey}&token=${apiToken}`,
        { name: newBoardName }
      );
      setBoards(prevState => [...prevState, response.data]);
      // setBoards(prevState => prevState.push(response.data));
      setNewBoardName("");
    } catch (error) {
      console.error("Error creating board:", error);
    }
  };

  return (
    <div style={{ marginLeft: "30px", marginRight: "30px" }}>
      <Typography variant="h4" gutterBottom>
        Boards
      </Typography>
      <Grid container spacing={2}>
        {boards.map((board) => (
          <Grid item key={board.id} xs={12} sm={6} md={4} lg={3}>
            <Card sx={{ marginBottom: "8px", backgroundColor: "lavender" }}>
              <CardContent>
                <Link to={`/boards/${board.id}`}>
                  <Typography variant="h6" component="div">
                    {board.name}
                  </Typography>
                </Link>
              </CardContent>
            </Card>
          </Grid>
        ))}
        <Grid item xs={12} sm={6} md={4} lg={3}>
          <Card sx={{ marginBottom: "8px" }}>
            <CardContent>
              <TextField
                label="New Board Name"
                value={newBoardName}
                onChange={(e) => setNewBoardName(e.target.value)}
                sx={{ marginBottom: "8px", marginRight: "10px" }}
              />
              <Button variant="contained" onClick={createBoard}>
                Create Board
              </Button>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}

export default Home;
