import { CssBaseline, Toolbar, Typography, AppBar } from "@mui/material"

const NavBar = () => {
  return (
    <>
    <CssBaseline />
    <AppBar position = 'relative' sx={{ marginBottom: "30px" }}>
        <Toolbar>
            <Typography variant="h4">
                Trello
            </Typography>
        </Toolbar>

    </AppBar>
    </>
  )
}

export default NavBar
